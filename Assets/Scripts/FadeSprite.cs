﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeSprite : MonoBehaviour {
    public bool fadeInAndOut, fadeIn, fadeOut;
    public float stayDuration, fadeDuration;

    public bool destroyOnDisappear; 

    private void OnEnable()
    {
        if(fadeInAndOut)
        {
            StartCoroutine(fadeImageInAndOut()); 
        } else if (fadeIn)
        {
            StartCoroutine(fadeImage(transform.GetComponent<SpriteRenderer>(), false)); 
        } else if (fadeOut)
        {
            StartCoroutine(fadeImage(transform.GetComponent<SpriteRenderer>(), true)); 
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines(); 
    }

    public IEnumerator fadeImageInAndOut()
    {
        while (true)
        {
            StartCoroutine(fadeImage(transform.GetComponent<SpriteRenderer>(), false));
            yield return new WaitForSeconds(fadeDuration);
            yield return new WaitForSeconds(stayDuration);
            StartCoroutine(fadeImage(transform.GetComponent<SpriteRenderer>(), true));
            yield return new WaitForSeconds(fadeDuration);
            yield return new WaitForSeconds(stayDuration); 
        }
    }

    public IEnumerator fadeImage(SpriteRenderer img, bool fadeAway)
    {
        Color color = img.color;

        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= 1/fadeDuration * Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
            if(destroyOnDisappear)
            {
                Destroy(gameObject); 
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += 1/fadeDuration * Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(color.r, color.g, color.b, i);
                yield return null;
            }
        }
    }
}
