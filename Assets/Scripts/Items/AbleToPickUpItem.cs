﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbleToPickUpItem : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision)
    {
        HandHandler activeHandHandler = GetComponent<HandsPicker>().activeHand.GetComponent<HandHandler>();
        if (collision.gameObject.CompareTag("Item") && !activeHandHandler.item) {
            if (collision.gameObject.GetComponent<Item>().pickUpAble)
            {
                activeHandHandler.item = collision.gameObject;
                collision.gameObject.GetComponent<Item>().pickUpAble = false; 
            }
        } 
    }
}
