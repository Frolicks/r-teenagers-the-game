﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargedItem : Item {

    public float maxCharges;
    public float charges;

    public float decayRate;
    public bool loseChargesOnDetach; 
    
    public override void use()
    {
        if (charges <= maxCharges)
        {
            charges++;
        }
    }

    private void Update()
    {
        if(charges > 0)
        {
            charges -= decayRate * Time.deltaTime;
        }


        if(loseChargesOnDetach && !owner)
        {
            charges = 0f;  
        }

        if(charges > maxCharges - 0.5f)
        {
            Camera.main.GetComponent<Shake>().shakeForSeconds(0.05f, 0.05f); 
        }
    }
}
