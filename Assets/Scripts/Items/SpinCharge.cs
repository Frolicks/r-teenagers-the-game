﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ChargedItem))]
public class SpinCharge : MonoBehaviour
{

    //spin handling
    private Spin spinScript;
    private ChargedItem chargedItem;

    private void Start()
    {
        spinScript = GetComponentInChildren<Spin>();
        chargedItem = GetComponentInChildren<ChargedItem>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GetComponentInChildren<BoxCollider2D>().isTrigger)
        {
            float decayRate = spinScript.decayRate;
            spinScript.speed = chargedItem.charges / chargedItem.maxCharges * (decayRate * chargedItem.maxCharges * 2);
        }

    }
}
