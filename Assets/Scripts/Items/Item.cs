﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
    public GameObject owner; 
    public bool pickUpAble;
    public float durability;

    public void activate()
    {
        MonoBehaviour[] components = GetComponents<MonoBehaviour>(); 
        foreach (MonoBehaviour mb in components) {
            mb.enabled = true; 
        }
    }

    public virtual void use()
    {
        
    }

   public void detach()
    {
        GetComponentInChildren<Collider2D>().isTrigger = false;
        owner.GetComponent<HandsPicker>().activeHandHandler.item = null; 
        owner = null;
        pickUpAble = true;  
    }

    public void attach(GameObject newOwner)
    {
        detach();
        owner = newOwner;
        GetComponentInChildren<Collider2D>().isTrigger = true;
        pickUpAble = false; 
    }
}


