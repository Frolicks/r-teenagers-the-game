﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEOFidgetSpinner : MonoBehaviour {

    private RemoteControl rc;
    private ChargedItem ci;  

	// Use this for initialization
	void Start () {

        rc = GetComponent<RemoteControl>();
        ci = GetComponent<ChargedItem>(); 
		
	}

    // Update is called once per frame
    void Update() {
        if (ci.charges >= ci.maxCharges - 3 && ci.owner)
        {
            rc.controlledSeconds = ci.charges;  
            //rc.grantControlSessionTo(ci.owner, ci.charges); 
        }
	}
}
