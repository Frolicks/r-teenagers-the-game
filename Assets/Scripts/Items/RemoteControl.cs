﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteControl : MonoBehaviour {

    public float speed; 

    public GameObject controller;

    public float controlledSeconds; 

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update()
    {             //placeholder remote control code - call from playerinputcontroller in future release 
        if (controlledSeconds > 0)
        {
            controlMoveTowards(controller, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        if(controlledSeconds > 0)
        {
            controlledSeconds -= Time.deltaTime; 
        }
	}

    public void controlMoveTowards(GameObject caster, Vector2 destination)
    {
        if(true) //caster == controller)
        {
            GetComponent<Rigidbody2D>().velocity = (destination - (Vector2)transform.position).normalized * speed; 
        }
    }

    public void grantControlSessionTo(GameObject person, float seconds)
    {
        if(true) //!controller)
        {
            StartCoroutine(grantControlSessionToCR(person, seconds));
            
        }
    }

    private IEnumerator grantControlSessionToCR(GameObject person, float seconds)
    {
        controller = person;
        yield return new WaitForSeconds(seconds);
        controller = null; 
    }
}
