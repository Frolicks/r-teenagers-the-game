﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogVelocity : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        Debug.Log(rb.velocity + "  magnitude: " + rb.velocity.magnitude + "    normalized: " + rb.velocity.normalized); 
	}
}
