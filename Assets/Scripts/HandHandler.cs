﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandHandler : MonoBehaviour {

    public GameObject item;



    private void Update()
    {
        if (item)
        {
            item.transform.position = transform.position;
            if (!item.GetComponentInChildren<BoxCollider2D>().isTrigger)
            {
                item.GetComponentInChildren<BoxCollider2D>().isTrigger = true; 
            }

        }
    }






}
