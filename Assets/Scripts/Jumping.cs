﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : MonoBehaviour {
    public float jumpForce, standingTimeBetweenJumps;
    public LayerMask WhatIsGround; 

    private Rigidbody2D rb;
    private Transform jumpPos;
    private float distToGround;
    private float standingTime; 

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        distToGround = GetComponent<BoxCollider2D>().bounds.extents.y;
        jumpPos = transform.GetChild(0);
    }
	
	// Update is called once per frame
	void Update () {
		if(grounded())
        {
            standingTime += Time.deltaTime; 
        }

        if (standingTime >= standingTimeBetweenJumps && grounded())
        {
            jump();
            standingTime = 0f; 
        }
	}

    public void jump()
    {
        rb.AddForce((jumpPos.position - transform.position).normalized * jumpForce); 
    }

    public bool grounded(){
        return Physics2D.Raycast(transform.position, -Vector3.up, distToGround + 0.1f, WhatIsGround);
    }
}
