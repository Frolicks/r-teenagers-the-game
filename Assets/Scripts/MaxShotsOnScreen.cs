﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxShotsOnScreen : MonoBehaviour {
    public int maximumShotsOnScreen;

    public static GameObject caster;
    public static int shotsOnScreen = 0; 
	// Use this for initialization
	void Start () {
        if (!caster) caster = GameObject.FindGameObjectWithTag("caster");

        shotsOnScreen++;

        if (shotsOnScreen == maximumShotsOnScreen)
        {
            //caster.GetComponentInChildren<>().loaded = false;
        } 
	}

    private void OnDestroy()
    {
        shotsOnScreen--; 
        if (shotsOnScreen < maximumShotsOnScreen)
        {
            //caster.GetComponentInChildren<ProjectileSpawner>().loaded = true;
        }
    }
}
