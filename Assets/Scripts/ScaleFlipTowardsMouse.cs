﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleFlipTowardsMouse : MonoBehaviour
{
    public bool twoDirectional;
    // Update is called once per frame
    void Update()
    {
        Vector2 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (twoDirectional)
        {
            if ((mouseWorldPos - (Vector2)transform.position).x < 0f)
            {
                if (transform.localScale.x > 0f)
                    transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z); 
            }
            else
            {
                if (transform.localScale.x < 0f)
                    transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
            }
        }


    }
}