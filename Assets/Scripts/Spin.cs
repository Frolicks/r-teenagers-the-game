﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {

    public float speed;
    public float decayRate;

    public bool decaying;  

    private RectTransform rt; 

	// Use this for initialization
	void Start () {
        decaying = true; 
        rt = GetComponent<RectTransform>();
	} 
	
	// Update is called once per frame
	void Update () {
        float visualSpeed = 0; 
        if(speed > 5000f)
        {
            visualSpeed = 5000f;  
        } else
        {
            visualSpeed = speed; 
        }
        
        if (rt)
            rt.rotation = Quaternion.Euler(0, 0, rt.rotation.eulerAngles.z + visualSpeed * Time.deltaTime);
        else
            transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + visualSpeed * Time.deltaTime);

        if(speed > 0 && decaying)
            speed -= decayRate * Time.deltaTime;
    }
}
