﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeOnCollision : MonoBehaviour {
    public GameObject explosion;
    public float requiredVelocity; 

    private void OnCollisionEnter2D(Collision2D collision)
    {
       if ( collision.relativeVelocity.magnitude > requiredVelocity)
        {
            //collision.gameObject.name != GetComponent<Projectile>().caster.name &&
            Instantiate(explosion, collision.contacts[0].point, Quaternion.identity);
            Camera.main.GetComponent<Shake>().shakeForSeconds(0.2f);
            Destroy(gameObject);
        } 
    }
}
