﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsPicker : MonoBehaviour {
    public float grabRange;
    public float throwForce;  
    public GameObject[] hands;

    public GameObject activeHand;

    public HandHandler activeHandHandler;

	// Use this for initialization
	void Start () {
        setActiveHand(hands[0]);
        activeHandHandler = activeHand.GetComponent<HandHandler>();
    }

    private void Update()
    {
        activeHandHandler = activeHand.GetComponent<HandHandler>();
    }

    public void toggleHands()
    {
        if(activeHand == hands[0])
        {
            setActiveHand(hands[1]); 
        } else
        {
            setActiveHand(hands[0]);  
        }
    }

    public void grabWithHand(GameObject obj)
    {
        bool withinReach = Vector2.Distance(obj.transform.position, activeHand.transform.position) < grabRange;

        if (!activeHandHandler.item && withinReach) 
        {
            if (obj.GetComponent<Item>().pickUpAble)
            {
                activeHandHandler.item = obj;
                activeHandHandler.item.GetComponent<Item>().owner = this.transform.root.gameObject;

                obj.GetComponent<Item>().pickUpAble = false;

            }
        }

    }

    public void dropItemAt(Vector2 idealDropPoint) 
    {
        if (activeHandHandler.item)
        {
            activeHandHandler.item.transform.position = activeHand.transform.position; //todo place range based on mouse
            activeHandHandler.item.GetComponent<Item>().detach(); 
            activeHandHandler.item = null;
        }
    }

    public void throwItem()
    {
        if(activeHandHandler.item)
        {
            activeHandHandler.item.GetComponent<Rigidbody2D>().AddForce(activeHand.transform.right * throwForce);
            activeHandHandler.item.GetComponentInChildren<Item>().detach(); 
            activeHandHandler.item = null;
        }
    }

    public void useItem()
    {
        if(activeHandHandler.item)
        {
            activeHandHandler.item.GetComponent<Item>().use(); 
        }
    }

    private void setActiveHand(GameObject hand) {
        if(activeHand)
            activeHand.transform.parent.GetComponent<LookAtMouse>().enabled = false;
        activeHand = hand; 
        activeHand.transform.parent.GetComponent<LookAtMouse>().enabled = true;
    }

    public void setActiveHand(int handNumber)
    {
        if(handNumber == 0)
        {
            setActiveHand(hands[0]); 
        } else
        {
            setActiveHand(hands[1]); 
        }
    }
}
