﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
    {
        public bool shaking;
        public float defaultShakeMagnitude;
        private float shakeMagnitude; 
        private Vector3 defaultPosition;
        private RectTransform rt;
        // Use this for initialization
        void Start()
        {
            rt = GetComponent<RectTransform>();

            if (rt)
            {
                defaultPosition = rt.position;
            }
            else
            {
                defaultPosition = transform.position;
            }

            shakeMagnitude = defaultShakeMagnitude; 

        }

        // Update is called once per frame
        void Update()
        {
            if (GetComponent<Camera2DFollow>())
            {
                defaultPosition = GetComponent<Camera2DFollow>().defaultPosition;
            }
            
            if (shaking)
            {
                if (rt)
                {
                    rt.position = Random.insideUnitSphere * shakeMagnitude + defaultPosition;
                }
                else
                {
                    transform.position = Random.insideUnitSphere * shakeMagnitude + defaultPosition;
                }
            }
            else
            {
                if (rt)
                {
                    rt.position = defaultPosition;
                }
                else
                {
                    transform.position = defaultPosition;
                }
            }
        }

        public void shakeForSeconds(float seconds)
        {
            StartCoroutine(shakeForSecondsCR(seconds));
        }

        public void shakeForSeconds(float seconds, float customShakeMagnitude)
        {
            StartCoroutine(shakeForSecondsCR(seconds, customShakeMagnitude));
        }

    private IEnumerator shakeForSecondsCR(float seconds)
        {
            shaking = true;
            yield return new WaitForSeconds(seconds);
            shaking = false;
        }
    private IEnumerator shakeForSecondsCR(float seconds, float customShakeMagnitude)
    {
        shakeMagnitude = customShakeMagnitude; 
        shaking = true;
        yield return new WaitForSeconds(seconds);
        shaking = false;
        shakeMagnitude = defaultShakeMagnitude; 
    }
}



