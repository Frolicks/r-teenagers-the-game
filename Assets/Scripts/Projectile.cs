﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    //public Vector2 mousePosAtBirth;
    public float speed;
    public GameObject caster; 

    protected Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //transform.right = (mousePosAtBirth - (Vector2)transform.position).normalized;
    }
    // Update is called once per frame

    void Update()
    {
        rb.velocity = transform.right * speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        string layerName = LayerMask.LayerToName(collision.gameObject.layer);
        switch (layerName)
        {
            case "Obstacle":
                speed = 0;
                gameObject.AddComponent<Lifetime>().lifetime = 3f; 
                break;
        }
    }

    
}
