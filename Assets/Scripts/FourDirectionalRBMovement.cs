﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourDirectionalRBMovement : MonoBehaviour {

    public float speed;

    [HideInInspector]
    public BoxCollider2D coll;
    [HideInInspector]
    public Vector2 playerInput; 

    private Rigidbody2D rb; 

	// Use this for initialization
	void Awake () {
        coll = GetComponent<BoxCollider2D>();  
        rb = GetComponent<Rigidbody2D>();
	}

    public void moveInDirection(Vector2 direction) 
    {
        rb.velocity = direction * speed;
    }
}

