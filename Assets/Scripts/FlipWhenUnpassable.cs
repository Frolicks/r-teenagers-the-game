﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipWhenUnpassable : MonoBehaviour {

    public Transform wallCheck;
    public LayerMask turnWhenFacedWith; 

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D objectAhead = Physics2D.Linecast(GetComponent<Rigidbody2D>().position, wallCheck.position, turnWhenFacedWith); 
        if (objectAhead)
        {
            flip();
        }

    }
    public void flip()
    {
        if (transform.localScale.x > 0f) { 
                transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
        }
        else
        {
            if (transform.localScale.x < 0f)
                transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
        }
    }

}
