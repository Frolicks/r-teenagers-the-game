﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnCollide : MonoBehaviour {
    public int damage;
    public float requiredVelocity;
     
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Health collidedHealth = collision.gameObject.GetComponent<Health>(); 
        if(collidedHealth)
        {
            collidedHealth.takeDamage(damage);  
            
        } 
    }
}
