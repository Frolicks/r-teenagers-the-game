﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnParticleEnd : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(GetComponentInChildren<ParticleSystem>().isStopped)
        {
            Destroy(gameObject); 
        }
	}
}
