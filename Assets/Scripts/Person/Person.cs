﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour {
    private HandsPicker handsPicker;
    private FourDirectionalRBMovement movement;

    public bool moveFreely;
    public float interactableRange;
    public GameObject activeItem;
    public GameObject targetFurniture;  

	// Use this for initialization
	void Start () {
        moveFreely = true;

        handsPicker = GetComponentInChildren<HandsPicker>();
        movement = GetComponentInChildren<FourDirectionalRBMovement>(); 
	}

    private void Update()
    {
        activeItem = handsPicker.activeHandHandler.item;
    }

    public void move(Vector2 direction)
    {
        if (moveFreely)
        {
            movement.moveInDirection(direction);
        }
    }

    public void interactWith()
    {
        bool targetFurnitureIsWithinRange = Vector2.Distance(transform.position, targetFurniture.transform.position) < interactableRange;
        GameObject withItem = activeItem; 
        if (!withItem)
        {
            withItem = this.gameObject;
        }

        if (targetFurnitureIsWithinRange)
        {
            targetFurniture.GetComponent<InteractableFurniture>().interact(withItem);
        }
    }
}
