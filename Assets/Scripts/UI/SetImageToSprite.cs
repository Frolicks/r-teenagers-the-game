﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  

public class SetImageToSprite : MonoBehaviour {
    public GameObject objectWithSprite;  

    private Image displayedImage;

    private void Start()
    {
        displayedImage = GetComponent<Image>();  
    }

    // Update is called once per frame
    void Update () {
        GameObject objectComprisedOfSprite = objectWithSprite.GetComponent<HandHandler>().item;
        if (objectComprisedOfSprite)
        {
            displayedImage.sprite = objectComprisedOfSprite.GetComponentInChildren<SpriteRenderer>().sprite;

            Vector3 spriteBoundsSize = displayedImage.sprite.bounds.size;
            GetComponent<AspectRatioFitter>().aspectRatio = spriteBoundsSize.x / spriteBoundsSize.y;
        } else
        {
            displayedImage.sprite = null;
            GetComponent<AspectRatioFitter>().aspectRatio = 1; 
        }


	}
}
