﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class ActiveHandIndicator : MonoBehaviour {

    public GameObject leftHand, rightHand;
    public GameObject leftHandDisplay, rightHandDisplay;
    private HandsPicker handPicker;  
	void Start () {
        handPicker = GameObject.Find("Player").GetComponent<HandsPicker>();  
	}
	
	// Update is called once per frame
	void Update () {
		if(handPicker.activeHand == leftHand)
        {
            rightHandDisplay.GetComponent<Image>().color = Color.gray; 
            leftHandDisplay.GetComponent<Image>().color = Color.black; 
        } else
        {
            leftHandDisplay.GetComponent<Image>().color = Color.gray; 
            rightHandDisplay.GetComponent<Image>().color = Color.black; 
        }
	}
}
