﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    public int maxHealth;
    public int currentHealth;

    public float invulnerableTime;

    public bool invulnerable;  

	// Use this for initialization
	void Start () {
        invulnerable = false; 
        if (currentHealth == 0)
        {
            currentHealth = maxHealth; 
        }
		
	}
	
	// Update is called once per frame
	void Update () {
        
		
	}

    public void takeDamage(int damage)
    {
        if(!invulnerable)
        {
            currentHealth -= damage;
            if (invulnerableTime > 0)
            {
                StartCoroutine(makeInvulnerable(invulnerableTime));
            }

            if(currentHealth == 0)
            {
                Destroy(gameObject); 
            }
        }

    }
    
    public void die()
    {
        //TODO death animation 
        Destroy(gameObject); 
    }

    private IEnumerator makeInvulnerable(float seconds)
    {
        invulnerable = true;
        yield return new WaitForSeconds(seconds);
        invulnerable = false; 
    }
}
