﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    private Person person; 

    public KeyCode useButton, interactButton;

    public float mouseOverRadius; 
    public ContactFilter2D mouseOverCF; 

    public HandsPicker handsPicker; 
    private Vector2 playerDirectionalInput;


    //interaction with environment
    private bool targetFurnitureIsWithinRange;

    private void Start()
    {
        person = GetComponent<Person>();  
        handsPicker = GetComponent<HandsPicker>();

    }

    void Update()
    {

        GameObject targetFurniture = person.targetFurniture; 
        playerDirectionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        handleMouseInput();
        handleHandSelection();
        handleMouseOver(); 

        if (Input.GetKeyDown(useButton))
        {
            handsPicker.useItem();  
        }


        if (targetFurniture)
        {
            targetFurnitureIsWithinRange = Vector2.Distance(transform.position, targetFurniture.transform.position) < person.interactableRange;

            //mouseover stuff
            GameObject interactionItem = handsPicker.activeHandHandler.item; 
            if(!interactionItem)
            {
                interactionItem = this.gameObject;  
            }

            targetFurniture.GetComponent<InteractableFurniture>().showMouseOverImage(interactionItem, targetFurnitureIsWithinRange);

            //interacting  
            if (Input.GetKeyDown(interactButton) && targetFurnitureIsWithinRange)
            {
               person.interactWith(); 
            }
            
        }
    }

    private void handleHandSelection()
    {
        
        if (Input.mouseScrollDelta.magnitude > 0)
        {
            GetComponent<HandsPicker>().toggleHands();
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            GetComponent<HandsPicker>().setActiveHand(0); 
        } 
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            GetComponent<HandsPicker>().setActiveHand(1); 
        }

    }

    public void handleMouseOver()
    {
        Collider2D[] itemsNearMouse = new Collider2D[10]; 
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Physics2D.OverlapCircle(mousePos, mouseOverRadius, mouseOverCF, itemsNearMouse);

        if(itemsNearMouse != null)
        {
            foreach (Collider2D cd in itemsNearMouse)
            {
                if(cd)
                {
                    InteractableFurniture interactableFurniture = cd.gameObject.GetComponent<InteractableFurniture>();

                    if (interactableFurniture)
                    {
                        person.targetFurniture = cd.gameObject;
                        return; 
                    }
                }
            }
            person.targetFurniture = null; 
        }
    }

    private void FixedUpdate()
    {
        GetComponent<Person>().move(playerDirectionalInput);
    }
    private void handleMouseInput()
    {
        if (Input.GetMouseButtonDown(1))
        {
            handsPicker.throwItem();
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (handsPicker.activeHandHandler.item)
            {
                GetComponent<HandsPicker>().dropItemAt((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));
                return;
            }
        }

        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit)
        {
            if (Input.GetMouseButtonDown(0))
            {
                GameObject rootObject = hit.collider.transform.root.gameObject;
                if (rootObject.CompareTag("Item"))
                {
                    GetComponent<HandsPicker>().grabWithHand(rootObject);
                }
            }
        }
    }
}
