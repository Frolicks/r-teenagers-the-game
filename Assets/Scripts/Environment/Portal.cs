﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
    public Transform end;
    //public LayerMask passableLayers; 


    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.transform.position = end.GetChild(0).transform.position; 
    }
}
