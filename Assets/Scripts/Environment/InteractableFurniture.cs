﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class InteractableFurniture : MonoBehaviour {

    public Image[] mouseOverImages;
     
    public virtual void showMouseOverImage(GameObject activeItem, bool interactable)
    {
        MouseOverCanvas canvas = GetComponentInParent<MouseOverCanvas>();
        canvas.enableCanvas();

        bool opaque = transform.root.GetComponentInChildren<Image>().color.a == 1;

        if (interactable)
        {
            if (!opaque)
            {

                Image[] images = canvas.GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    Color color = image.color;
                    color.a = 1f;
                    image.color = color;
                }
            }
        }
        else
        {
            if (opaque)
            {
                Image[] images = canvas.GetComponentsInChildren<Image>();
                foreach (Image image in images)
                {
                    Color color = image.color;
                    color.a = 0.5f;
                    image.color = color;
                }
            }
        }
        
        canvas.canvasVisibleTime = 0.05f; 
    }
     
    public virtual void interact(GameObject activeItem)
    {
        
    }
}
