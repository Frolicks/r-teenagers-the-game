﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table : InteractableFurniture {

    public GameObject satOnBy;
    public GameObject itemOnTable;

    public GameObject chair;

    private void Start()
    {
        chair = transform.parent.Find("Chair").gameObject; 
    }

    private void Update()
    {
        if(satOnBy && Vector2.Distance(satOnBy.transform.position, chair.transform.position) > 1)
        {
            satOnBy = null; 
        }
    }

    public override void showMouseOverImage(GameObject activeItem, bool interactable)
    {
        base.showMouseOverImage(activeItem, interactable);

        if(activeItem.GetComponent<Person>())
        {
            if (activeItem.transform.root.gameObject != satOnBy)
            {
                //todo sit icon 
            }
            else
            {
                //todo eat icon
            }
        }
            
        if(activeItem.GetComponent<Item>())
        {
            //TODO place on table icon 

        }
        
    }

    public override void interact(GameObject activeItem)
    {

        if (activeItem.GetComponent<Person>())
        {
            if (activeItem.transform.root.gameObject != satOnBy)
            {
                satOnBy = activeItem.transform.root.gameObject;
                satOnBy.transform.position = chair.transform.position;
            } else
            {
                //todo eat case
            }

        }

        if (activeItem.GetComponent<Item>())
        {
            Item item = activeItem.GetComponent<Item>();
            item.attach(transform.parent.gameObject);

            itemOnTable = item.gameObject;
            itemOnTable.transform.position = transform.position;  
        }
    }
}
