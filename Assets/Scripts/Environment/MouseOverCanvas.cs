﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOverCanvas : MonoBehaviour
{
    public Canvas canvas;
    public float canvasVisibleTime; 

    // Use this for initialization
    void Start()
    {
        
        if (!canvas)
        {
            canvas = GetComponentInChildren<Canvas>();
        }
        canvasVisibleTime = 0f; 
    }

    public void disableCanvas()
    {
        canvas.gameObject.SetActive(false); 
    }
    
    public void enableCanvas()
    {
        canvas.gameObject.SetActive(true); 
    }

    public void Update()
    {
        if (canvasVisibleTime > 0)
        {
            canvasVisibleTime -= Time.deltaTime;
        }
        else
        {
            disableCanvas();    
        }
    }


}
