﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDetector : MonoBehaviour {
    public Collider2D[] items = new Collider2D[3];
    public ContactFilter2D detectionFilter;  
    
	// Use this for initialization
	void Start () {
        StartCoroutine(detectItems()); 
	}
	
	// Update is called once per frame
	void Update () {


    }

    private IEnumerator detectItems()
    {
        while (true)
        {
            GetComponent<Collider2D>().OverlapCollider(detectionFilter, items);


            foreach (Collider2D cd in items)
            {
                Debug.Log(cd);
            }

            yield return new WaitForSeconds(0.5f); 
        }

        
    }
}
