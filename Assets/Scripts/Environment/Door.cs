﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : InteractableFurniture {
    public bool opensDown;  
    public bool open;

    public float openForce; 

    private HingeJoint2D hinge;
	// Use this for initialization
	void Start () {
        hinge = GetComponent<HingeJoint2D>();
        hinge.useMotor = !open; 

        if(!opensDown)
        {
            JointMotor2D motor = hinge.motor;
            motor.motorSpeed = -motor.motorSpeed;
            hinge.motor = motor;  


            JointAngleLimits2D limits = hinge.limits;
            limits.max = -limits.max;  
            hinge.limits = limits;  

            openForce = -openForce;
        }
	}

    public override void showMouseOverImage(GameObject activeItem, bool interactable)
    {
        base.showMouseOverImage(activeItem, interactable);
    }

    public override void interact(GameObject activeItem)
    {
        if (activeItem.GetComponent<Person>())    // nothing in hand
        {
            toggleOpen();
        }
    }

    public void toggleOpen()
    {
        open = !open;
        hinge.useMotor = !open; 
        if(open)
        {
            GetComponent<Rigidbody2D>().AddTorque(openForce); 
        }
    }
}
