﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpriteFacesMouse : MonoBehaviour {
    public bool twoDirectional; 

	// Update is called once per frame
	void Update () {
        Vector2 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);  

        if(twoDirectional)
        {
            if ((mouseWorldPos - (Vector2)transform.position).x < 0f)
            {
                GetComponent<SpriteRenderer>().flipX = true;
            } else
            {
                GetComponent<SpriteRenderer>().flipX = false;
            }
        }

		
	}
}
